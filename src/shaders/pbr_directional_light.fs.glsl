#version 330

in vec3 vViewSpaceNormal;
in vec3 vViewSpacePosition;
in vec2 vTexCoords;

in vec3 vTangentLighDir;
in vec3 vTangentCameraPos;
in vec3 vTangentFragPos;
in mat3 TBN;
in vec3 DEBUG;

uniform vec3 uLightDirection;
uniform vec3 uLightIntensity;

uniform sampler2D uBaseColorTexture;
uniform sampler2D uMetallicRoughnessTexture;
uniform sampler2D uEmissiveTexture;
uniform sampler2D uOcclusionTexture;
uniform sampler2D uNormalTexture;

uniform vec4 uBaseColorFactor;
uniform float uRoughnessFactor;
uniform float uMetallicFactor;
uniform vec3 uEmissiveFactor;
uniform float uOcclusionStrength;
uniform int uOcclusionEnabled;
uniform float uNormalScale;
uniform int uUseNormalMap;
uniform int uShowNormals;

uniform mat4 uNormalMatrix;

out vec3 fColor;

// Constants
const float GAMMA = 2.2;
const float INV_GAMMA = 1. / GAMMA;
const float M_PI = 3.141592653589793;
const float M_1_PI = 1.0 / M_PI;


vec3 LINEARtoSRGB(vec3 color) 
{ 
    return pow(color, vec3(INV_GAMMA)); 
}

vec4 SRGBtoLINEAR(vec4 srgbIn)
{
    return vec4(pow(srgbIn.xyz, vec3(GAMMA)), srgbIn.w);
}

float Heaviside(float x) { return x > 0 ? 1 : 0; }

void main()
{
    // normal mapping
    // -------------------------------------------------------------------------------------------------
    vec3 N;
    if (uUseNormalMap == 1)
    {
        N = texture(uNormalTexture, vTexCoords).rgb;
        N = N * 2.0 - 1.0;   
        N = normalize(TBN * N);
    }
    else
    {
        N = normalize(vViewSpaceNormal);
    }

    if (uShowNormals == 1)
    {
        fColor = N;
        return;
    }

    vec3 L = uLightDirection;
    vec3 V = normalize(-vViewSpacePosition);
    vec3 H = normalize(L + V);
    float NdotL = clamp(dot(N, L), 0., 1.);
    float HdotL = clamp(dot(H, L), 0., 1.);
    float HdotV = clamp(dot(H, V), 0., 1.);
    float NdotV = clamp(dot(N, V), 0., 1.);
    float NdotH = clamp(dot(H, N), 0., 1.);

    vec4 baseColorFromTexture = SRGBtoLINEAR(texture(uBaseColorTexture, vTexCoords));
    vec4 baseColor = uBaseColorFactor * baseColorFromTexture;
    
    float metallic = uMetallicFactor * texture(uMetallicRoughnessTexture, vTexCoords).b;
    float roughness = uRoughnessFactor * texture(uMetallicRoughnessTexture, vTexCoords).g;
    float alpha = roughness * roughness;
    float alphaSqr = alpha * alpha;
    vec3 dielectricSpecular = vec3(0.04);



    // spec brdf
    // -------------------------------------------------------------------------------------------------
    float numV = Heaviside(HdotL) * Heaviside(HdotV);
    float denomV = abs(NdotL) + sqrt(alphaSqr + (1 - alphaSqr) * NdotL * NdotL);
    denomV *= NdotV + sqrt(alphaSqr + (1 - alphaSqr) * NdotV * NdotV);

    float Vspec = denomV != 0.0 ? numV / denomV : 0.0;
    float numD = alphaSqr * Heaviside(NdotH);
    float denomD = M_PI * (NdotH * NdotH * (alphaSqr - 1.) + 1.) * (NdotH * NdotH * (alphaSqr - 1) + 1);
    float Dspec = denomD != 0.0 ? numD / denomD : 0.0;
    
    vec3 specular_brdf = vec3(Vspec * Dspec);

    // diffuse brdf
    // -------------------------------------------------------------------------------------------------
    vec3 black = vec3(0.);
    vec3 c_diff = mix(baseColor.rgb * (1. - dielectricSpecular.r), black, metallic);
    vec3 diffuse_brdf = M_1_PI * c_diff;

    // fresnel
    // -------------------------------------------------------------------------------------------------
    vec3 f_0 = mix(vec3(dielectricSpecular), baseColor.rgb, metallic);
    float baseShlickFactor = 1. - HdotV;
    float shlickFactor = baseShlickFactor * baseShlickFactor; // power 2
    shlickFactor *= shlickFactor; // power 4
    shlickFactor *= baseShlickFactor; // power 5
    vec3 fresnel = f_0 + (vec3(1.) - f_0) * shlickFactor;

    // emissive
    // -------------------------------------------------------------------------------------------------
    vec3 emissive = SRGBtoLINEAR(texture(uEmissiveTexture, vTexCoords)).rgb * uEmissiveFactor;

    // final color
    // -------------------------------------------------------------------------------------------------
    vec3 f_diffuse = (1. - fresnel) * diffuse_brdf;
    vec3 f_specular = specular_brdf * fresnel;
    vec3 color = (f_diffuse + f_specular) * uLightIntensity * NdotL + emissive;

    // occlusion
    // -------------------------------------------------------------------------------------------------
    if (uOcclusionEnabled == 1) 
    {
        float occlusion = SRGBtoLINEAR(texture(uOcclusionTexture, vTexCoords)).r;
        color = mix(color, color * occlusion, 1.0);
    }
    
    fColor = LINEARtoSRGB(color);
}
