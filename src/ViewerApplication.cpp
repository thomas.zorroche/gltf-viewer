#include "ViewerApplication.hpp"

#include <iostream>
#include <numeric>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/io.hpp>

#include "utils/cameras.hpp"
#include "utils/gltf.hpp"

#include <stb_image_write.h>

void keyCallback(GLFWwindow *window, int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE) {
        glfwSetWindowShouldClose(window, 1);
    }
}

int ViewerApplication::run()
{
    // Loader shaders
    const auto wireframeProgram = compileProgram({m_ShadersRootPath / m_wireframeVertexShader, m_ShadersRootPath / m_wireframeFragmentShader});
    const auto modelViewProjMatrixLocationWireframe = glGetUniformLocation(wireframeProgram.glId(), "uModelViewProjMatrix");
    
    const auto glslProgram = compileProgram({m_ShadersRootPath / m_vertexShader, m_ShadersRootPath / m_fragmentShader});

    const auto modelViewProjMatrixLocation = glGetUniformLocation(glslProgram.glId(), "uModelViewProjMatrix");
    const auto modelViewMatrixLocation = glGetUniformLocation(glslProgram.glId(), "uModelViewMatrix");
    const auto modelMatrixLocation = glGetUniformLocation(glslProgram.glId(), "uModelMatrix");
    const auto normalMatrixLocation = glGetUniformLocation(glslProgram.glId(), "uNormalMatrix");
    const auto lightDirectionLocation = glGetUniformLocation(glslProgram.glId(), "uLightDirection");
    const auto lightIntensityLocation = glGetUniformLocation(glslProgram.glId(), "uLightIntensity");
    
    const auto uBaseColorTexture= glGetUniformLocation(glslProgram.glId(), "uBaseColorTexture");
    const auto uMetallicRoughnessTexture = glGetUniformLocation(glslProgram.glId(), "uMetallicRoughnessTexture");
    const auto uEmissiveTexture = glGetUniformLocation(glslProgram.glId(), "uEmissiveTexture");
    const auto uOcclusionTexture = glGetUniformLocation(glslProgram.glId(), "uOcclusionTexture");
    const auto uNormalTexture = glGetUniformLocation(glslProgram.glId(), "uNormalTexture");
    
    const auto uBaseColorFactor = glGetUniformLocation(glslProgram.glId(), "uBaseColorFactor");
    const auto uRoughnessFactor = glGetUniformLocation(glslProgram.glId(), "uRoughnessFactor");
    const auto uMetallicFactor = glGetUniformLocation(glslProgram.glId(), "uMetallicFactor");
    const auto uEmissiveFactor = glGetUniformLocation(glslProgram.glId(), "uEmissiveFactor");
    const auto uOcclusionStrength = glGetUniformLocation(glslProgram.glId(), "uOclussionFactor");
    const auto uOcclusionEnabled = glGetUniformLocation(glslProgram.glId(), "uOcclusionEnabled");
    const auto uNormalScale = glGetUniformLocation(glslProgram.glId(), "uNormalScale");
    const auto uUseNormalMap = glGetUniformLocation(glslProgram.glId(), "uUseNormalMap");
    const auto uShowNormals = glGetUniformLocation(glslProgram.glId(), "uShowNormals");
    
    const auto uCameraPos = glGetUniformLocation(glslProgram.glId(), "uCameraPos");

    // Light Setup
    glm::vec3 lightDirection = { 1, 1, 1 };
    glm::vec3 lightIntensity = { 0.8f, 0.8f, 0.8f };
    bool lightFromCamera = false;
    bool occlusionEnabled = true;
    bool useNormalMap = false;
    bool showNormals = false;
    bool showWireframe = false;

    tinygltf::Model model;
    if (loadGltfFile(model) != 0)
        return -1;

    glm::vec3 bboxMin;
    glm::vec3 bboxMax;
    computeSceneBounds(model, bboxMin, bboxMax);
    glm::vec3 bboxCenter = (bboxMin + bboxMax) * 0.5f;
    glm::vec3 bboxDiag = bboxMax - bboxMin;

    // Build projection matrix
    auto maxDistance = glm::length(bboxDiag);
    maxDistance = maxDistance == 0.f ? maxDistance : 20.f;
    const auto projMatrix = glm::perspective(70.f, float(m_nWindowWidth) / m_nWindowHeight, 0.001f * maxDistance, 1.5f * maxDistance);

    std::unique_ptr<CameraController> cameraController = std::make_unique<TrackballCameraController>(m_GLFWHandle.window(), 0.1f * maxDistance);
    if (m_hasUserCamera) 
    {
        cameraController->setCamera(m_userCamera);
    } 
    else 
    {
        const glm::vec3 eye = bboxDiag.z > 0 ? bboxCenter + bboxDiag : bboxCenter + 2.f * glm::cross(bboxDiag, glm::vec3(0, 1, 0));
        cameraController->setCamera(Camera{ eye, bboxCenter, glm::vec3(0, 1, 0) });
    }

    // Creation of Buffer Objects and Creation of Vertex Array Objects
    std::vector<VaoRange> meshIndexToVaoRange;
    std::vector<GLuint> VAOs = CreateBuffers(model, meshIndexToVaoRange);
    std::cout << "VAOs count : " << VAOs.size() << std::endl;

    // Creation of textures
    std::vector<GLuint> textureObjects = createTextureObjects(model);

    // Create white texture
    GLuint whiteTexture;
    float white[] = {1, 1, 1, 1};
    glGenTextures(1, &whiteTexture);
    glBindTexture(GL_TEXTURE_2D, whiteTexture);
    {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 1, 1, 0, GL_RGBA, GL_FLOAT, white);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_REPEAT);
    }
    glBindTexture(GL_TEXTURE_2D, 0);


    // Setup OpenGL state for rendering
    glEnable(GL_DEPTH_TEST);
    glslProgram.use();

    // Lambda function to bind material
    const auto bindMaterial = [&](const auto materialIndex) {
        if (materialIndex >= 0)
        {
            const auto &material = model.materials[materialIndex];
            const auto &pbrMetallicRoughness = material.pbrMetallicRoughness;

            if (pbrMetallicRoughness.baseColorTexture.index >= 0)
            {
                // Base Color Texture
                const auto &textureColor = model.textures[pbrMetallicRoughness.baseColorTexture.index];
                glActiveTexture(GL_TEXTURE0);
                glBindTexture(GL_TEXTURE_2D, textureObjects[textureColor.source]);
                glUniform1i(uBaseColorTexture, 0);
                glUniform4f(uBaseColorFactor, 
                    (float)pbrMetallicRoughness.baseColorFactor[0],
                    (float)pbrMetallicRoughness.baseColorFactor[1],
                    (float)pbrMetallicRoughness.baseColorFactor[2],
                    (float)pbrMetallicRoughness.baseColorFactor[3]);

                // Metallic Roughness Texture
                auto textureMetallicObject = 0u;
                if (pbrMetallicRoughness.metallicRoughnessTexture.index >= 0) 
                {
                    const auto &textureMetallic = model.textures[pbrMetallicRoughness.metallicRoughnessTexture.index];
                    if (textureMetallic.source >= 0)
                        textureMetallicObject = textureObjects[textureMetallic.source];
                }
                
                glActiveTexture(GL_TEXTURE1);
                glBindTexture( GL_TEXTURE_2D, textureMetallicObject);
                glUniform1i(uMetallicRoughnessTexture, 1);
                glUniform1f(uMetallicFactor, (float)pbrMetallicRoughness.metallicFactor);
                glUniform1f(uRoughnessFactor, (float)pbrMetallicRoughness.roughnessFactor);

                // Emissive Texture
                auto textureEmissiveObject = 0u;
                if (material.emissiveTexture.index >= 0) 
                {
                    const auto &textureEmissive = model.textures[material.emissiveTexture.index];
                    if (textureEmissive.source >= 0)
                        textureEmissiveObject = textureObjects[textureEmissive.source];
                }
                glActiveTexture(GL_TEXTURE2);
                glBindTexture(GL_TEXTURE_2D, textureEmissiveObject);
                glUniform1i(uEmissiveTexture, 2);
                glUniform3f(uEmissiveFactor, (float)material.emissiveFactor[0], (float)material.emissiveFactor[1], (float)material.emissiveFactor[2]);


                // Occlusion Texture
                auto textureOcclusionObject = whiteTexture;
                if (material.occlusionTexture.index >= 0) 
                {
                    const auto &textureOcclusion = model.textures[material.occlusionTexture.index];
                    if (textureOcclusion.source >= 0)
                        textureOcclusionObject = textureObjects[textureOcclusion.source];
                }
                glActiveTexture(GL_TEXTURE3);
                glBindTexture(GL_TEXTURE_2D, textureOcclusionObject);
                glUniform1i(uOcclusionTexture, 3);
                glUniform1f(uOcclusionStrength, (float)material.occlusionTexture.strength);
                glUniform1i(uOcclusionEnabled, occlusionEnabled);

                // Normal Texture
                auto textureNormalObject = 0u;
                if (material.normalTexture.index >= 0) 
                {
                    const auto &textureNormal = model.textures[material.normalTexture.index];
                    if (textureNormal.source >= 0)
                        textureNormalObject = textureObjects[textureNormal.source];
                }
                glActiveTexture(GL_TEXTURE4);
                glBindTexture(GL_TEXTURE_2D, textureNormalObject);
                glUniform1i(uNormalTexture, 4);
                glUniform1i(uUseNormalMap, useNormalMap);
                glUniform1i(uShowNormals, showNormals);
                glUniform1f(uNormalScale, (float)material.normalTexture.scale);
            }
        }
        else
        {
            // Bind white texture
            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, whiteTexture);
            glUniform1i(uBaseColorTexture, 0);
            glUniform4f(uBaseColorFactor, 1, 1, 1, 1);
        }
    };

    // Lambda function to draw the scene
    const auto drawScene = [&](const Camera &camera) {
        glViewport(0, 0, m_nWindowWidth, m_nWindowHeight);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        const auto viewMatrix = camera.getViewMatrix();

        // The recursive function that should draw a node
        // We use a std::function because a simple lambda cannot be recursive
        const std::function<void(int, const glm::mat4 &)> drawNode =
            [&](int nodeIdx, const glm::mat4 &parentMatrix) {
                // The drawNode function
                const tinygltf::Node& node = model.nodes[nodeIdx];
                const glm::mat4 modelMatrix = getLocalToWorldMatrix(node, parentMatrix);

                if (node.mesh >= 0)
                {
                    glUniform3f(uCameraPos, camera.eye().x, camera.eye().y, camera.eye().z);

                    const glm::mat4 modelViewMatrix = viewMatrix * modelMatrix;
                    const glm::mat4 modelViewProjectionMatrix = projMatrix * modelViewMatrix;
                    const glm::mat4 normalMatrix = glm::transpose(glm::inverse(modelViewMatrix)); 

                    glUniformMatrix4fv(modelMatrixLocation, 1, GL_FALSE, &modelMatrix[0][0]);
                    glUniformMatrix4fv(modelViewProjMatrixLocation, 1, GL_FALSE, &modelViewProjectionMatrix[0][0]);
                    glUniformMatrix4fv(modelViewMatrixLocation, 1, GL_FALSE, &modelViewMatrix[0][0]);
                    glUniformMatrix4fv(normalMatrixLocation, 1, GL_FALSE, &normalMatrix[0][0]);

                    glm::vec3 uLightDirection = lightFromCamera ? glm::vec3(0, 0, 1) : glm::normalize(viewMatrix * glm::vec4(lightDirection, 0));
                    glUniform3f(lightDirectionLocation, uLightDirection.x, uLightDirection.y, uLightDirection.z);
                    glUniform3f(lightIntensityLocation, lightIntensity.x, lightIntensity.y, lightIntensity.z);

                    const tinygltf::Mesh& mesh = model.meshes[node.mesh]; 
                    VaoRange vaoRange = meshIndexToVaoRange[node.mesh];
                    for (int id_p = 0; id_p < mesh.primitives.size(); id_p++)
                    {
                        GLuint VAO = VAOs[vaoRange.begin + id_p];
                        const tinygltf::Primitive& primitive = mesh.primitives[id_p];

                        bindMaterial(primitive.material);
                        glBindVertexArray(VAO);
                        if (primitive.indices >= 0)
                        {
                            GLsizei num_indices = model.accessors[primitive.indices].count;
                            glDrawElements(primitive.mode, num_indices, model.accessors[primitive.indices].componentType, NULL);

                            if (showWireframe)
                            {
                                wireframeProgram.use();
                                glUniformMatrix4fv(modelViewProjMatrixLocationWireframe, 1, GL_FALSE, &modelViewProjectionMatrix[0][0]);
                                
                                glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
                                glDrawElements(primitive.mode, num_indices, model.accessors[primitive.indices].componentType, NULL);
                                glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

                                glslProgram.use();
                            }
                        }
                        else
                        {
                            const auto accessorIdx = (*begin(primitive.attributes)).second;
                            const auto &accessor = model.accessors[accessorIdx];
                            glDrawArrays(primitive.mode, 0, GLsizei(accessor.count));

                            if (showWireframe)
                            {
                                wireframeProgram.use();
                                glUniformMatrix4fv(modelViewProjMatrixLocationWireframe, 1, GL_FALSE, &modelViewProjectionMatrix[0][0]);
                                
                                glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
                                glDrawArrays(primitive.mode, 0, GLsizei(accessor.count));
                                glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

                                glslProgram.use();
                            }
                        }

                    }
                }

                if (node.children.size() >= 0)
                {
                    for (auto child : node.children)
                        drawNode(child, glm::mat4(1));
                }
            };

        // Draw the scene referenced by gltf file
        if (model.defaultScene >= 0) {
            // Draw all nodes
            for (auto nodeIdx : model.scenes[model.defaultScene].nodes)
            {
                drawNode(nodeIdx, glm::mat4(1));
            }
        }
    };

    // Loop until the user closes the window
    for (auto iterationCount = 0u; !m_GLFWHandle.shouldClose(); ++iterationCount) 
    {
        const auto seconds = glfwGetTime();

        const auto camera = cameraController->getCamera();
        drawScene(camera);

        // ImGUI Code
        drawImGUIFrame(cameraController, lightDirection, lightIntensity, 
            lightFromCamera, occlusionEnabled, useNormalMap, showNormals, showWireframe);

        glfwPollEvents(); // Poll for and process events

        auto ellapsedTime = glfwGetTime() - seconds;
        auto guiHasFocus = ImGui::GetIO().WantCaptureMouse || ImGui::GetIO().WantCaptureKeyboard;
        if (!guiHasFocus) {
            cameraController->update(float(ellapsedTime));
        }

        m_GLFWHandle.swapBuffers(); // Swap front and back buffers
    }

    // TODO clean up allocated GL data

    return 0;
}

ViewerApplication::ViewerApplication(const fs::path &appPath, uint32_t width,
    uint32_t height, const fs::path &gltfFile,
    const std::vector<float> &lookatArgs, const std::string &vertexShader,
    const std::string &fragmentShader, const fs::path &output) :
    m_nWindowWidth(width),
    m_nWindowHeight(height),
    m_AppPath{appPath},
    m_AppName{m_AppPath.stem().string()},
    m_ImGuiIniFilename{m_AppName + ".imgui.ini"},
    m_ShadersRootPath{m_AppPath.parent_path() / "shaders"},
    m_gltfFilePath{gltfFile},
    m_OutputPath{output}
{
    if (!lookatArgs.empty()) {
        m_hasUserCamera = true;
        m_userCamera = Camera{glm::vec3(lookatArgs[0], lookatArgs[1], lookatArgs[2]),
                              glm::vec3(lookatArgs[3], lookatArgs[4], lookatArgs[5]),
                              glm::vec3(lookatArgs[6], lookatArgs[7], lookatArgs[8])};
    }

    if (!vertexShader.empty()) {
        m_vertexShader = vertexShader;
    }

    if (!fragmentShader.empty()) {
        m_fragmentShader = fragmentShader;
    }

    ImGui::GetIO().IniFilename = m_ImGuiIniFilename.c_str(); // At exit, ImGUI will store its windows
                                                             // positions in this file

    glfwSetKeyCallback(m_GLFWHandle.window(), keyCallback);

    printGLVersion();
}

void ViewerApplication::drawImGUIFrame(std::unique_ptr<CameraController>& cameraController, 
    glm::vec3& lightDirection, glm::vec3& lightIntensity, bool& lightFromCamera, 
    bool& occlusionEnabled, bool& useNormalMap, bool& showNormals, bool& showWireframe)
{
    const auto camera = cameraController->getCamera();

    imguiNewFrame();
    {
        ImGui::Begin("GUI");
        ImGui::Text("Application average %.3f ms/frame (%.1f FPS)",
                1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);

        if (ImGui::CollapsingHeader("Camera", ImGuiTreeNodeFlags_DefaultOpen)) 
        {
            if (ImGui::TreeNode("Coordinates"))
            {
                ImGui::Text("eye: %.3f %.3f %.3f", camera.eye().x, camera.eye().y, camera.eye().z);
                ImGui::Text("center: %.3f %.3f %.3f", camera.center().x, camera.center().y, camera.center().z);
                ImGui::Text("up: %.3f %.3f %.3f", camera.up().x, camera.up().y, camera.up().z);
                ImGui::Text("front: %.3f %.3f %.3f", camera.front().x, camera.front().y, camera.front().z);
                ImGui::Text("left: %.3f %.3f %.3f", camera.left().x, camera.left().y,camera.left().z);
                ImGui::TreePop();
            }

            if (ImGui::Button("Center View"))
            {
                cameraController->centerView();
            }

            static int cameraControllerID = 1;
            if (ImGui::RadioButton("First Person", &cameraControllerID, 0))
            {
                const auto& saveCamera = cameraController->getCamera();
                cameraController = std::make_unique<FirstPersonCameraController>(
                    m_GLFWHandle.window(), 
                    cameraController->getSpeed(),
                    cameraController->getWorldUpAxis());
                cameraController->setCamera(saveCamera);
            } 
            ImGui::SameLine();
            if (ImGui::RadioButton("Trackball", &cameraControllerID, 1))
            {
                const auto& saveCamera = cameraController->getCamera();
                cameraController = std::make_unique<TrackballCameraController>(
                    m_GLFWHandle.window(), 
                    cameraController->getSpeed(),
                    cameraController->getWorldUpAxis());
                cameraController->setCamera(saveCamera);
            }

            ImGui::DragFloat("Camera Speed", &cameraController->getSpeed(), 0.1f, 1.0f, 10.0f);

            if (ImGui::Button("CLI camera args to clipboard")) {
                std::stringstream ss;
                ss << "--lookat " << camera.eye().x << "," << camera.eye().y << ","
                        << camera.eye().z << "," << camera.center().x << ","
                        << camera.center().y << "," << camera.center().z << ","
                        << camera.up().x << "," << camera.up().y << "," << camera.up().z;
                const auto str = ss.str();
                glfwSetClipboardString(m_GLFWHandle.window(), str.c_str());
            }
        }

        if (ImGui::CollapsingHeader("Lighting", ImGuiTreeNodeFlags_DefaultOpen)) 
        {
            static float intensity = 1.0f;
            static glm::vec3 lightColor = lightIntensity;
            if (ImGui::DragFloat("Intensity", &intensity, 0.1f, 0.0f, 15.0f))
            {
                lightIntensity = intensity * lightColor;
            }
            if (ImGui::ColorEdit3("Color", &lightColor[0]))
            {
                lightIntensity = lightColor;
            }
            ImGui::DragFloat3("Direction", &lightDirection[0], 0.1f, 0.0f, 360.0f);

            ImGui::Checkbox("Light from camera", &lightFromCamera);
            ImGui::Checkbox("Use Occlusion", &occlusionEnabled);
            ImGui::Checkbox("Use NormalMap", &useNormalMap);
            ImGui::Checkbox("Show Normals", &showNormals);
            ImGui::Checkbox("Show Wireframe", &showWireframe);
        }

        ImGui::End();
    }
    imguiRenderFrame();
}


int ViewerApplication::loadGltfFile(tinygltf::Model& model)
{
    tinygltf::TinyGLTF loader;
    std::string err;
    std::string warn;

    bool ret = loader.LoadASCIIFromFile(&model, &err, &warn, m_gltfFilePath.string());
    //bool ret = loader.LoadBinaryFromFile(&model, &err, &warn, m_gltfFilePath); // for binary glTF(.glb)

    if (!warn.empty()) {
        printf("Warn: %s\n", warn.c_str());
    }

    if (!err.empty()) {
        printf("Err: %s\n", err.c_str());
    }

    if (!ret) {
        printf("Failed to parse glTF\n");
        return -1;
    }

    return 0;
}

std::vector<GLuint> ViewerApplication::CreateBuffers(tinygltf::Model& model, std::vector<VaoRange> & meshIndexToVaoRange)
{
    std::vector<GLuint> VAOs;

    const GLuint VERTEX_ATTRIB_POSITION_IDX = 0;
    const GLuint VERTEX_ATTRIB_NORMAL_IDX = 1;
    const GLuint VERTEX_ATTRIB_TEXCOORD0_IDX = 2;
    const GLuint VERTEX_ATTRIB_TANGENT_IDX = 3;

    size_t meshesCount = model.meshes.size();
    meshIndexToVaoRange.resize(meshesCount);

    // Create VBOs
    for (int id_m = 0; id_m < meshesCount; id_m++)
    {
        size_t primitivesCount = model.meshes[id_m].primitives.size();
        const auto vaoOffset = VAOs.size();

        VaoRange &vaoRange = meshIndexToVaoRange[id_m];
        vaoRange.begin = GLsizei(vaoOffset);
        vaoRange.count = vaoOffset + primitivesCount;
        
        VAOs.resize(vaoOffset + primitivesCount);
        glGenVertexArrays(primitivesCount, &VAOs[vaoOffset]);
        for (int id_p = 0; id_p < primitivesCount; id_p++)
        {
            const GLuint VAO = VAOs[vaoRange.begin + id_p];
            glBindVertexArray(VAO);

            GLuint VBO = 0;
            glGenBuffers(1, &VBO);
            glBindBuffer(GL_ARRAY_BUFFER, VBO);

            // Positions
            const auto iteratorPos = model.meshes[id_m].primitives[id_p].attributes.find("POSITION");
            if (iteratorPos == end(model.meshes[id_m].primitives[id_p].attributes)) 
            { 
                std::cerr << "ERROR: POSITION NOT FIND" << std::endl;
            }
            tinygltf::Accessor positionAccessor = model.accessors[model.meshes[id_m].primitives[id_p].attributes["POSITION"]];
            size_t positionByteLength = model.bufferViews[positionAccessor.bufferView].byteLength;
            size_t positionByteOffset = model.bufferViews[positionAccessor.bufferView].byteOffset;
            size_t positionByteStride = model.bufferViews[positionAccessor.bufferView].byteStride;
            int positionBuffer = model.bufferViews[positionAccessor.bufferView].buffer;
            glEnableVertexAttribArray(VERTEX_ATTRIB_POSITION_IDX);
            glVertexAttribPointer(VERTEX_ATTRIB_POSITION_IDX, positionAccessor.type, positionAccessor.componentType, GL_FALSE, positionByteStride, NULL);

            // Normals
            const auto iteratorNormals = model.meshes[id_m].primitives[id_p].attributes.find("NORMAL");
            if (iteratorNormals == end(model.meshes[id_m].primitives[id_p].attributes)) 
            {
                std::cerr << "ERROR: NORMALS NOT FIND" << std::endl;
            }
            tinygltf::Accessor normalsAccessor = model.accessors[model.meshes[id_m].primitives[id_p].attributes["NORMAL"]];
            size_t normalByteLength = model.bufferViews[normalsAccessor.bufferView].byteLength;
            size_t normalByteOffset = model.bufferViews[normalsAccessor.bufferView].byteOffset;
            size_t normalByteStride = model.bufferViews[normalsAccessor.bufferView].byteStride;
            int normalBuffer = model.bufferViews[normalsAccessor.bufferView].buffer;
            glEnableVertexAttribArray(VERTEX_ATTRIB_NORMAL_IDX);
            glVertexAttribPointer(VERTEX_ATTRIB_NORMAL_IDX, normalsAccessor.type, normalsAccessor.componentType, GL_FALSE, normalByteStride, (GLvoid*)positionByteLength);

            // Tex coords
            const auto iteratorTexCoords = model.meshes[id_m].primitives[id_p].attributes.find("TEXCOORD_0");
            if (iteratorTexCoords == end(model.meshes[id_m].primitives[id_p].attributes)) 
            {
                // TODO : some model do not have tex coords (white texture)
                std::cerr << "ERROR: TEX COORDS NOT FIND" << std::endl;
            }
            tinygltf::Accessor texAccessor = model.accessors[model.meshes[id_m].primitives[id_p].attributes["TEXCOORD_0"]];
            size_t texByteLength = model.bufferViews[texAccessor.bufferView].byteLength;
            size_t texByteOffset = model.bufferViews[texAccessor.bufferView].byteOffset;
            size_t texByteStride = model.bufferViews[texAccessor.bufferView].byteStride;
            int texBuffer         = model.bufferViews[texAccessor.bufferView].buffer;
            glEnableVertexAttribArray(VERTEX_ATTRIB_TEXCOORD0_IDX);
            glVertexAttribPointer(VERTEX_ATTRIB_TEXCOORD0_IDX, texAccessor.type, texAccessor.componentType, GL_FALSE, GLsizei(texByteStride), (const GLvoid *)(positionByteLength + normalByteLength));

            // Tangents
            const auto iteratorTangents = model.meshes[id_m].primitives[id_p].attributes.find("TANGENT");
            std::vector<glm::vec4> tangentsBuffer;
            if (iteratorTangents == end(model.meshes[id_m].primitives[id_p].attributes)) 
            {
                std::cerr << "ERROR: TANGENTS NOT FIND" << std::endl;
            }
            else 
            {
                //
                //  WORK IN PROGRESS
                // 
                //int tangentID = 0;
                //if (model.meshes[id_m].primitives[id_p].indices >= 0)
                //{
                //    tinygltf::Accessor indicesAccessor = model.accessors[model.meshes[id_m].primitives[id_p].indices];
                //    int indicesByteBuffer = model.bufferViews[indicesAccessor.bufferView].buffer;
                //    size_t indicesByteOffset = model.bufferViews[indicesAccessor.bufferView].byteOffset;
                //    for (size_t i = 0; i < indicesAccessor.count; ++i)
                //    {
                //        uint32_t index = 0;
                //        switch (indicesAccessor.componentType) {
                //            case TINYGLTF_COMPONENT_TYPE_UNSIGNED_BYTE:
                //                index = *((const uint8_t *)&indicesByteBuffer.data[indicesByteOffset + indicesAccessor.bufferView.byteStride * i]);
                //                break;
                //            case TINYGLTF_COMPONENT_TYPE_UNSIGNED_SHORT:
                //                index = *((const uint16_t *)&indicesByteBuffer.data[indicesByteOffset + indexByteStride * i]);
                //                break;
                //            case TINYGLTF_COMPONENT_TYPE_UNSIGNED_INT:
                //                index = *((const uint32_t *)&indicesByteBuffer.data[indicesByteOffset + indexByteStride * i]);
                //            break;
                //        }
                //        const auto &v0 = *((const glm::vec3 *)&positionBuffer.data[positionByteOffset + positionByteStride * index]);
                //    }
                //}
                //else
                //{
                //    for (size_t i = 0; i < positionAccessor.count; i+=3)
                //    {
                //        const auto &v0 = *((const glm::vec3 *)&model.buffers[positionBuffer].data[positionByteOffset + positionByteStride * i]);
                //        const auto &v1 = *((const glm::vec3 *)&model.buffers[positionBuffer].data[positionByteOffset + positionByteStride * (i + 1)]);
                //        const auto &v2 = *((const glm::vec3 *)&model.buffers[positionBuffer].data[positionByteOffset + positionByteStride * (i + 2)]);

                //        // Shortcuts for UVs
                //        const glm::vec2 & uv0 = *((const glm::vec2 *)&model.buffers[texBuffer].data[texByteOffset + texByteStride * i]);
                //        const glm::vec2 & uv1 = *((const glm::vec2 *)&model.buffers[texBuffer].data[texByteOffset + texByteStride * (i + 1)]);
                //        const glm::vec2 & uv2 = *((const glm::vec2 *)&model.buffers[texBuffer].data[texByteOffset + texByteStride * (i + 2)]);

                //        // Edges of the triangle : position delta
                //        glm::vec3 deltaPos1 = v1-v0;
                //        glm::vec3 deltaPos2 = v2-v0;

                //        // UV delta
                //        glm::vec2 deltaUV1 = uv1-uv0;
                //        glm::vec2 deltaUV2 = uv2-uv0; 

                //        float r = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV1.y * deltaUV2.x);
                //        glm::vec3 tangent = (deltaPos1 * deltaUV2.y   - deltaPos2 * deltaUV1.y)*r;

                //        tangentsBuffer.push_back(glm::vec4(tangent, 1.0f));
                //        tangentsBuffer.push_back(glm::vec4(tangent, 1.0f));
                //        tangentsBuffer.push_back(glm::vec4(tangent, 1.0f));
                //        tangentID++;
                //    }
                //}
            }

            tinygltf::Accessor tangentAccessor = model.accessors[model.meshes[id_m].primitives[id_p].attributes["TANGENT"]];
            size_t tangentByteLength = model.bufferViews[tangentAccessor.bufferView].byteLength;
            size_t tangentByteOffset = model.bufferViews[tangentAccessor.bufferView].byteOffset;
            size_t tangentByteStride = model.bufferViews[tangentAccessor.bufferView].byteStride;
            int tangentBuffer = model.bufferViews[tangentAccessor.bufferView].buffer;
            glEnableVertexAttribArray(VERTEX_ATTRIB_TANGENT_IDX);
            glVertexAttribPointer(VERTEX_ATTRIB_TANGENT_IDX, 4, tangentAccessor.componentType, GL_FALSE, GLsizei(tangentByteStride), (const GLvoid*)(positionByteLength + normalByteLength + texByteLength));

            // Fill VBO
            glBufferData(GL_ARRAY_BUFFER, positionByteLength + normalByteLength + texByteLength + tangentByteLength, NULL, GL_STATIC_DRAW);
            glBufferSubData(GL_ARRAY_BUFFER, 0, positionByteLength, (void*)(model.buffers[positionBuffer].data.data() + positionByteOffset + positionAccessor.byteOffset));
	        glBufferSubData(GL_ARRAY_BUFFER, positionByteLength, normalByteLength, (void*)(model.buffers[normalBuffer].data.data() + normalByteOffset + normalsAccessor.byteOffset));
	        glBufferSubData(GL_ARRAY_BUFFER, normalByteLength + positionByteLength, texByteLength, (void*)(model.buffers[texBuffer].data.data() + texByteOffset + texAccessor.byteOffset));
	        
            glBufferSubData(GL_ARRAY_BUFFER, normalByteLength + positionByteLength + texByteLength, tangentByteLength, (void*)(model.buffers[tangentBuffer].data.data() + tangentByteOffset + tangentAccessor.byteOffset));
            //if (iteratorTangents == end(model.meshes[id_m].primitives[id_p].attributes)) 
            //    glBufferSubData(GL_ARRAY_BUFFER, normalByteLength + positionByteLength + texByteLength, tangentByteLength, (void*)(model.buffers[tangentBuffer].data.data() + tangentByteOffset + tangentAccessor.byteOffset));
            //else
            //    glBufferSubData(GL_ARRAY_BUFFER, normalByteLength + positionByteLength + texByteLength, tangentByteLength, (void*)(tangentsBuffer.data() + tangentByteOffset + tangentAccessor.byteOffset));


            // Indices
            if (model.meshes[id_m].primitives[id_p].indices >= 0) 
            {
                GLuint IBO = 0;
                glGenBuffers(1, &IBO);

                tinygltf::Accessor indicesAccessor = model.accessors[model.meshes[id_m].primitives[id_p].indices];
                size_t indicesByteLength = model.bufferViews[indicesAccessor.bufferView].byteLength;
                size_t indicesByteOffset = model.bufferViews[indicesAccessor.bufferView].byteOffset;
                int indicesByteBuffer = model.bufferViews[indicesAccessor.bufferView].buffer;
                size_t indexCount = indicesAccessor.count;
                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);
                glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesByteLength, (void*)(model.buffers[indicesByteBuffer].data.data() + indicesByteOffset+indicesAccessor.byteOffset), GL_STATIC_DRAW);
            }
            
            glBindVertexArray(0);
        }
    }

    return VAOs;
}

std::vector<GLuint> ViewerApplication::createTextureObjects(const tinygltf::Model &model) const
{
    std::vector<GLuint> texturesObjects(model.textures.size(), 0);
    //texturesObjects.resize(model.textures.size(), 0);
    glGenTextures(GLsizei(model.textures.size()), texturesObjects.data());

    tinygltf::Sampler defaultSampler;
    defaultSampler.minFilter = GL_LINEAR;
    defaultSampler.magFilter = GL_LINEAR;
    defaultSampler.wrapS = GL_REPEAT;
    defaultSampler.wrapT = GL_REPEAT;
    defaultSampler.wrapR = GL_REPEAT;

    for (int texIdx = 0; texIdx < model.textures.size(); texIdx++)
    {
        const auto &texture = model.textures[texIdx];
        assert(texture.source >= 0); // ensure a source image is present
        const auto &image = model.images[texture.source]; // get the image
        const auto &sampler = texture.sampler >= 0 ? model.samplers[texture.sampler] : defaultSampler;

        // fill the texture object with the data from the image
        glBindTexture(GL_TEXTURE_2D, texturesObjects[texIdx]);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image.width, image.height, 0, GL_RGBA, image.pixel_type, image.image.data());
        //texturesObjects.push_back(texture.source);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, sampler.minFilter != -1 ? sampler.minFilter : GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, sampler.magFilter != -1 ? sampler.magFilter : GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, sampler.wrapS);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, sampler.wrapT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, sampler.wrapR);

        // Filters
        if (sampler.minFilter == GL_NEAREST_MIPMAP_NEAREST ||
            sampler.minFilter == GL_NEAREST_MIPMAP_LINEAR ||
            sampler.minFilter == GL_LINEAR_MIPMAP_NEAREST ||
            sampler.minFilter == GL_LINEAR_MIPMAP_LINEAR) {
            glGenerateMipmap(GL_TEXTURE_2D);
        }
    }

    glBindTexture(GL_TEXTURE_2D, 0);
    return texturesObjects;
}


